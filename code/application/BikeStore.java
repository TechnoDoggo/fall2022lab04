//Edward Barbura 2133700
package application;
import vehicles.*;
public class BikeStore
{
    public static void main(String args[])
    {
        Bicycle[] BikeStock = new Bicycle[4];
        vehicles.Bicycle BicycleOne = new Bicycle("Bowser Bikes",7, 29);
        vehicles.Bicycle BicycleTwo = new Bicycle("Mauve Trails", 10, 36.8);
        vehicles.Bicycle BicycleThree = new Bicycle("Dog Gone Nature", 12, 50);
        vehicles.Bicycle BicycleFour = new Bicycle("Casual Bikes", 5, 20);

        BikeStock[0] = BicycleOne;
        BikeStock[1] = BicycleTwo;
        BikeStock[2] = BicycleThree;
        BikeStock[3] = BicycleFour;

        for (int i = 0; i<BikeStock.length; i++)
        {
            System.out.println(BikeStock[i]);
        }
    }
}