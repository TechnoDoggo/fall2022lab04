//Edward Barbura 2133700
package vehicles;
public class Bicycle
{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    /**
     * Takes the input and makes a new Bicycle object
     * 
     * @param newManufacturer The input of the Manufactuer given by the user
     * @param newNumberGears The input of the Number of Gears given by the user
     * @param newMaxSpeed The input of the max Speed given by the user
     */
    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed)
    {
        manufacturer = newManufacturer;
        numberGears = newNumberGears;
        maxSpeed = newMaxSpeed;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public int getNumberGears()
    {
        return numberGears;
    }

    public double getMaxSpeed()
    {
        return maxSpeed;
    }

    public String toString()
    {
        return "Manufaturer: " + getManufacturer() + ", Number of Gears: " + getNumberGears() + ", Max Speed: " + getMaxSpeed() + " km/h";
    }
}